public class DoublyLinkedList<T extends Comparable<T> > implements List {
    private Node<T> rootNode;
    private int size;
    private Node head;


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {

        return size==0;
    }

    @Override
    public Comparable get(int index) {
        int currentPos=0;
        for(Node node =rootNode;node!= null;node=node.getNext()){
            if(currentPos==index){
                return node.getData();
            }
            currentPos++;
        }
       return null;
    }
    public Comparable get(int index,Node node){
        if(index==0)return node.getData();
        return get(index -1,node.getNext());

    }
    @Override
    public void mergeSort() {
        head = RecursiveMergeSort(head);

    }
    private Node splitNodeInHalf(Node head) {
        Node fast = head, slow = head;
        while (fast.getNext() != null && fast.getNext().getNext() != null) {
            fast = fast.getNext().getNext();
            slow = slow.getNext();
        }
        Node temp = slow.getNext();
        slow.setNext(null);
        return temp;
    }

    private Node RecursiveMergeSort(Node node) {
        if (node.getNext() == null) {
            return node;
        }
        Node right = splitNodeInHalf(node);
        Node left = node;

        left = RecursiveMergeSort(left);
        right = RecursiveMergeSort(right);

        return mergeSortedNodes(left, right);
    }

    private Node mergeSortedNodes(Node first, Node second) {
        if (first == null) {
            return second;
        }
        if (second == null) {
            return first;
        }
        if (second.getData().compareTo(first.getData()) > 0) {
            Node mergedNextAndSecond = mergeSortedNodes(first.getNext(), second);
            first.setNext(mergedNextAndSecond);
            first.getNext().setPrev(first);
            first.setPrev(null);
            return first;
        } else {
            Node mergedNextAndFirst = mergeSortedNodes(first, second.getNext());
            second.setNext(mergedNextAndFirst);
            second.getNext().setPrev(second);
            second.setPrev(null);
            return second;
        }
    }


    @Override
    public boolean add(Comparable data) {
        Node<T> node = new Node<>(data);
        if(rootNode != null){
            rootNode.setPrev(node);
        }
        node.setNext(rootNode);
        rootNode=node;
        size++;
        return true;
    }
    @Override
    public Object[] toArray() {
        Node<T> root = head ;
        Object[] nodeToArray = new Object[size];
        int counter = 0;
        while (root != null) {
            nodeToArray[counter] = root.getData();
            counter ++;
            root = root.getNext();
        }
        return nodeToArray;
    }
    

    @Override
    public boolean remove(Comparable data) {
        return false;
    }


    @Override
    public void selectionSort() {

    }

    @Override
    public void bubbleSort() {

    }

    }



