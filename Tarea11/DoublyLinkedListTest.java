import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class DoublyLinkedListTest {

    @Test
    public void sizeTest(){
       DoublyLinkedList<Integer> dlist= new DoublyLinkedList();
       dlist.add(2);
       dlist.add(5);
        assertEquals(2,dlist.size());
    }
    @Test
    public void sizeTest2(){
        DoublyLinkedList<String> dlist2= new DoublyLinkedList();
        dlist2.add(2);
        dlist2.add(5);
        assertEquals(2,dlist2.size());
    }
    @Test
    public void emptyTest(){
        DoublyLinkedList<String> dlist3= new DoublyLinkedList();
        assertEquals(0,dlist3.size());
    }
    @Test
    public void emptyTest2(){
        DoublyLinkedList<Integer> dlist4= new DoublyLinkedList();
        assertEquals(0,dlist4.size());
    }
    @Test
    public void getTest(){
        DoublyLinkedList<Integer> dlist5= new DoublyLinkedList();
        dlist5.add(9);
        dlist5.add(7);
        dlist5.add(8);
        dlist5.add(3);
        dlist5.add(6);

        assertEquals(3,dlist5.get(1));
    }
    @Test
    public void getTest2(){
        DoublyLinkedList<String> dlist6= new DoublyLinkedList();
        dlist6.add("s");
        dlist6.add("r");
        dlist6.add("p");
        dlist6.add("u");
        dlist6.add("w");

        assertEquals("u",dlist6.get(1));
    }
    @Test
    public void mergeSortTest(){

        List<Integer> dlist7 = new DoublyLinkedList();
        dlist7.add(5);
        dlist7.add(1);
        dlist7.add(2);
        dlist7.add(3);
        dlist7.mergeSort();
        List<Integer> dlist7ordered = new DoublyLinkedList();
        dlist7.add(5);
        dlist7.add(3);
        dlist7.add(2);
        dlist7.add(1);
        assertEquals(dlist7.toString(),dlist7ordered.toString());
    }



}