package clase0811.bags;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    /*
    change the x element position to y position and
    y element position to x position
    0 1 2 3 4
    1,2,3,4,5
    xchange(1,3) =>
    1,4,3,2,5
    * */
    void xchange(int y, int x);

}
class Node<T extends Comparable<T>>  {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}

class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    public void selectionSort() {
        Node node = root;
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            T indexData = (T) node.getData();
            Node nextNode = node.getNext();
            for (int j = i + 1; j < size; j++) {
                T nextData = (T) nextNode.getData();
                if (indexData.compareTo(nextData) > 0) {
                    indexMin = j;
                    indexData = nextData;
                }
                nextNode = nextNode.getNext();
            }
            xchange(indexMin, i);
            node = getNode(i).getNext();
        }
    }

    public void bubbleSort() {
        boolean notSorted;
        do {
            notSorted = false;
            Node node = root;
            for (int i = 0; i < size - 1; i++) {
                T data = (T) node.getData();
                T nextData = (T) node.getNext().getData();
                if (data.compareTo(nextData) > 0) {
                    xchange(i, i + 1);
                    notSorted = true;
                }
                node = getNode(i).getNext();
            }
        } while (notSorted);

    }

    @Override
    public void xchange(int y, int x) {
        if (x == y || y >= size || x >= size || x < 0 || y < 0) { return; }
        T xData = getNode(x).getData();
        T yData = getNode(y).getData();
        setNode(x, yData);
        setNode(y, xData);
    }
    private Node<T> setNodeData(int pos, Node node, T data, int index) {
        if (pos == index) {
            Node<T> newNode = new Node(data);
            newNode.setNext(node.getNext());
            return newNode;
        }
        index++;
        Node nextNode = setNodeData(pos, node.getNext(), data, index);
        node.setNext(nextNode);
        return node;
    }

    private void setNode(int pos, T data) {
        root = setNodeData(pos, root, data, 0);
    }

    private Node<T> getNode(int pos) {
        Node node = root;
        for (int i = 0; i < size; i++) {
            if (i == pos) {
                return node;
            }
            node = node.getNext();
        }
        return node;
    }




}