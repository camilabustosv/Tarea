package clase0811.bags;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedBagTest {

    @Test
    void selectionSortTest(){
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(4);
        bag.selectionSort();
        Bag<Integer> orderedBag = new LinkedBag<>();
        orderedBag.add(5);
        orderedBag.add(4);
        orderedBag.add(3);
        orderedBag.add(2);
        orderedBag.add(1);
        assertEquals(orderedBag.toString(),bag.toString());

    }
    @Test
    void bubbleSortTest(){
        Bag<Integer> bag2 = new LinkedBag<>();
        bag2.add(5);
        bag2.add(3);
        bag2.add(1);
        bag2.add(2);
        bag2.add(4);
        bag2.bubbleSort();
        Bag<Integer> orderedBag2 = new LinkedBag<>();
        orderedBag2.add(5);
        orderedBag2.add(4);
        orderedBag2.add(3);
        orderedBag2.add(2);
        orderedBag2.add(1);
        assertEquals(orderedBag2.toString(),bag2.toString());

    }

}