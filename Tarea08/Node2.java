public class Node2 {
    int data;
    Node2 next = null;

    Node2( int data) {
        this.data = data;
    }

    public Node2(int data, Node2 next) {
        this.data = data;
        this.next = next;
    }

    public static Node2 append(Node2 listA, Node2 listB) {
        Node2 result = new Node2(-1);
        Node2 p = result;

        while (listA != null && listB != null) {
            if (listA.data <= listB.data) {
                p.next = listA;
                listA = listA.next;
            }

            else {
                p.next = listB;
                listB = listB.next;
            }
            p = p.next;
        }
        if (listA == null) {
            p.next = listB;
        }

        else if (listB == null) {
            p.next = listA;
        }

        return result.next;
    }
}
