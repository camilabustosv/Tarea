public class Solution {
        public static int sumGroups(int[] arr) {
            if (arr.length > 0) {
                int n, sum,auxSum;
                auxSum = sum = n = arr[0] & 1;
                int a = 1;
                int b = 0;
                for (int i = 1; i < arr.length; i++) {
                    if (n == (arr[i] & 1)) {
                        sum = (sum + n) & 1;
                    } else {
                        a += (auxSum ^ sum) & b;
                        auxSum = sum;
                        sum = n = arr[i] & 1;
                        b = 1;
                    }
                }
                a += (auxSum ^ sum) & b;
                return a;
            }
            return 0;
        }
    }

