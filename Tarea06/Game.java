import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

    public class Game {
        private final Scanner scanner = new Scanner(System.in);
        private final Random random = new Random();
        private final Messages messages = new Messages();
        protected String[][] map = new String[10][10];

        protected Game(){
            for (String[] strings : map) {
                Arrays.fill(strings, "-");
            }
            obstaclesPosition();
            playerPosition();
        }

        private int generateNumberRandom(){
            return random.nextInt(9);
        }

        private void obstaclesPosition(){
            map[generateNumberRandom()][generateNumberRandom()] = "\uD83D\uDE08";
            map[generateNumberRandom()][generateNumberRandom()] = "\uD83D\uDE08";
            map[generateNumberRandom()][generateNumberRandom()] = "\uD83D\uDE08";
            map[generateNumberRandom()][generateNumberRandom()] = "\uD83D\uDE08";
            map[generateNumberRandom()][generateNumberRandom()] = "\uD83D\uDE08";
        }

        public void playerPosition(){
            int positionY = generateNumberRandom();
            int positionX = generateNumberRandom();
            map[positionY][positionX] = "\uD83D\uDE42";
        }

        public int[] gameCoordinates(){
            int[] playerPositions = new int[2];
            for(int i = 0; i < map.length; i++){
                for(int j = 0; j < map[0].length; j++){
                    if (Objects.equals(map[i][j], "\uD83D\uDE42")){
                        playerPositions[0]= j;
                        playerPositions[1]= i;
                    }
                }
            }
            return playerPositions;
        }

        public void printMap(){

            for (String[] strings : map){
                System.out.println(Arrays.toString(strings));
            }
            System.out.println("\n");
        }

        public int verifyWinner(){
            int playerPositionX = gameCoordinates()[0];
            int playerPositionY = gameCoordinates()[1];
            if(playerPositionX == 9 && playerPositionY == 9){
                messages.showCongratulationsMessage();
            }
            int move = 0;
            return move;
        }

        public void moveUp(){
            int playerCoordinateX = gameCoordinates()[0];
            int playerCoodinatesY = gameCoordinates()[1];
            if (playerCoodinatesY != 0 && !map[playerCoodinatesY - 1][playerCoordinateX].equals("x")){
                map[playerCoodinatesY-1][playerCoordinateX] = "\uD83D\uDE42";
                map[playerCoodinatesY][playerCoordinateX] = "-";
            }
        }

        public void moveDown(){
            int playerCoordinateX = gameCoordinates()[0];
            int playerCoodinatesY = gameCoordinates()[1];
            if (playerCoodinatesY != map.length-1 && !map[playerCoodinatesY][playerCoordinateX].equals("x")){
                map[playerCoodinatesY+1][playerCoordinateX] = "\uD83D\uDE42";
                map[playerCoodinatesY][playerCoordinateX] = "-";
            }
        }

        public void moveLeft(){
            int playerCoordinateX = gameCoordinates()[0];
            int playerCoodinatesY = gameCoordinates()[1];
            if (playerCoordinateX != 0 && !map[playerCoodinatesY][playerCoordinateX -1].equals("x")){
                map[playerCoodinatesY][playerCoordinateX -1] = "\uD83D\uDE42";
                map[playerCoodinatesY][playerCoordinateX] = "-";
            }
        }

        public void moveRight(){
            int playerCoordinateX = gameCoordinates()[0];
            int playerCoodinatesY = gameCoordinates()[1];
            if (playerCoordinateX != map[0].length -1 && !map[playerCoodinatesY][playerCoordinateX +1].equals("x")){
                map[playerCoodinatesY][playerCoordinateX +1] = "\uD83D\uDE42";
                map[playerCoodinatesY][playerCoordinateX] = "-";
            }
        }

        public void movePlayer(){
            int move;
            printMap();
            move = scanner.nextInt();
            while (move != 0) {
                switch (move) {
                    case 1:
                        moveUp();
                        printMap();
                        break;
                    case 2:
                        moveDown();
                        printMap();
                        break;
                    case 3:
                        moveLeft();
                        printMap();
                        break;
                    case 4:
                        moveRight();
                        printMap();
                        break;
                    default:
                        messages.showErrorMessage();
                        break;
                }
                verifyWinner();
                move = scanner.nextInt();
            }
        }

        public void controller(){
            movePlayer();
        }

    }

