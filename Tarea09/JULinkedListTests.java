
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class JULinkedListTests {
    List<String> lista = new JULinkedList<String>();
    List<Integer> lista2 = new JULinkedList<>();

    @Test
    public void isEmptyTest(){
      lista2.add(2);
      lista2.add(3);
      lista2.isEmpty();
      assertEquals(false,lista2.isEmpty());

    }

    @Test
    public void addTest(){
        lista2.add(2);
        lista2.add(3);
        assertEquals(true,lista2.add(1));

    }
    @Test
    public void removeTest(){
        lista2.add(2);
        lista2.add(3);
        assertEquals(null,lista2.remove(3));

    }
    @Test
    public void clearTest(){
        lista2.add(2);
        lista2.add(3);
        lista2.clear();
        assertEquals(0,lista2.size());

    }
    @Test

    public void getTest(){
        lista2.add(2);
        lista2.add(5);
        lista2.add(6);
        lista2.add(7);
        lista2.get(5);
        assertEquals(4,lista2.size());
    }
    @Test

    public void addIndexTest(){
        lista2.add(2);
        lista2.add(5);
        lista2.add(6);
        lista2.add(7);
        lista2.add(3,6);
        assertEquals(3,lista2.size()-1);
    }

















}
