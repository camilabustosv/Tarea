import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {
        private int longitud;
        private Node<T> raiz;
        public int size() {
                return longitud;
        }
        public boolean vacia() {
                return raiz == null;
        }

        @Override
        public boolean isEmpty() {
                return raiz == null;
        }

        @Override
        public boolean contains(Object o) {
                return false;
        }

        @Override
        public Iterator<T> iterator() {
                return null;
        }

        @Override
        public Object[] toArray() {
                return new Object[0];
        }


        @Override
        public <T1> T1[] toArray(T1[] a) {
                return null;
        }

        @Override
        public boolean add(T dato) {
                Node<T> nodo = new Node<>(dato);
                if (vacia()) raiz = nodo;
                else {
                        Node<T> ultimo = obtenerNodo(longitud - 1);
                        ultimo.sig = nodo;
                }
                longitud++;
                return true;
        }
        @Override
        public boolean remove(Object dato) {
                if (vacia()) return false;
                if (raiz.dato.equals(dato)) {
                        raiz = raiz.sig;
                        longitud--;
                        return true;
                }
                for (Node<T> anterior = raiz; anterior.sig != null; anterior = anterior.sig) {
                        if (anterior.sig.dato.equals(dato)) {
                                anterior.sig = anterior.sig.sig;
                                longitud--;
                                return true;
                        }
                }
                return false;
        }



        @Override
        public boolean containsAll(Collection<?> c) {
                return false;
        }

        @Override
        public boolean addAll(Collection<? extends T> c) {
                return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends T> c) {
                return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
                return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
                return false;
        }

        @Override
        public void clear() {
                raiz = null;
                longitud = 0;

        }

        @Override
        public T get(int index) {
                Node<T> nodo = obtenerNodo(index);
                return nodo == null ? null : nodo.dato;
        }

        @Override
        public T set(int index, T element) {
                return null;
        }

        @Override
        public void add(int index, T element) {
                Node<T> nodo1 = raiz;
                Node<T> nodoNext = nodo1.sig;
                for (int i = 0; i < index; i++) {
                        nodo1 = nodo1.sig;
                        nodoNext = nodo1.sig;
                }
                nodo1.sig = new Node<>(element);
                nodo1.sig.sig = nodoNext;

        }

        @Override
        public T remove(int index) {
                return null;
        }

        @Override
        public int indexOf(Object o) {
                return 0;
        }


        /*public int indexOf(Object target) {
                Node node = raiz;
                for (int i=0; i<longitud; i++) {
                        if (equals(target, node.dato)) {
                                return i;
                        }
                        node = node.sig;
                }
                return -1;
        }

         */

        @Override
        public int lastIndexOf(Object o) {
                int index = -1;
                for (int i = longitud - 1; i >= 0; i--){
                        if (o.equals(get(i))){
                                index = i;
                                break;
                        }
                }
                return index;
        }

        @Override
        public ListIterator<T> listIterator() {
                return null;
        }

        @Override
        public ListIterator<T> listIterator(int index) {
                return null;
        }


        public Node<T> obtenerNodo(int index) {
                if (vacia() || index >= longitud)
                        return null;
                Node<T> nodo = raiz;
                for (int i = 0; i < index; i++, nodo = nodo.sig);
                return nodo;
        }

        @Override
        public List<T> subList(int fromIndex, int toIndex) {
                return null;
        }

       /* public Object[] toArray() {
                Object[] array = new Object[longitud];
                for (int i = 0; i < longitud; i++) array[i] = get(i).dato;

                return array;
        }

        */


}
