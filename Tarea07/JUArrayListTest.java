
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

    public class JUArrayListTest {
        JUArrayList<Integer> numbers;

        @BeforeEach
        public void init(){
            numbers = new JUArrayList<>();
        }

        @Test
        public void addSizeGetTest(){
            numbers.add(1);
            numbers.add(2);
            System.out.println(numbers);

            assertEquals(2, numbers.size());
            assertEquals(1, numbers.get(0));
            assertEquals(2, numbers.get(1));
        }

        @Test
        public void isEmptyTest(){
            assertTrue(numbers.isEmpty());

            numbers.add(1);
            numbers.add(2);
            System.out.println(numbers);
            assertFalse(numbers.isEmpty());

            numbers.clear();
            System.out.println(numbers);
            assertTrue(numbers.isEmpty());
        }

        @Test
        public void iteratorTest(){
            numbers.add(1);
            numbers.add(2);
            numbers.add(3);

            int iteratorCounter = 0;
            Iterator<Integer> iterator = numbers.iterator();
            while(iterator.hasNext()) {
                assertEquals(numbers.get(iteratorCounter), iterator.next());
                iteratorCounter++;
            }
            assertEquals(3, iteratorCounter);
        }

        @Test
        public void addByIndexTest(){
            numbers.add(1);
            numbers.add(2);
            numbers.add(7);
            numbers.add(1, 5);

            System.out.println(numbers);
            assertEquals(4, numbers.size());
            assertEquals(1, numbers.get(0));
            assertEquals(5, numbers.get(1));
            assertEquals(2, numbers.get(2));
            assertEquals(7, numbers.get(3));
        }

        @Test
        public void removeTest(){
            numbers.add(1);
            numbers.add(5);
            numbers.remove(numbers.get(0));

            System.out.println(numbers);

            assertEquals(1, numbers.size());
            assertEquals(5, numbers.get(0));
        }

        @Test
        public void removeByIndexTest(){
            numbers.add(2);
            numbers.add(5);
            numbers.add(7);
            Integer removed = numbers.remove(1);

            System.out.println(numbers);

            assertEquals(5, removed);
            assertEquals(2, numbers.size());
            assertEquals(7, numbers.get(1));
        }

        @Test
        public void clearTest(){
            numbers.add(2);
            numbers.add(5);
            numbers.add(7);
            System.out.println(numbers);

            numbers.clear();
            System.out.println(numbers);
            assertEquals(0, numbers.size());


            numbers.add(1);
            numbers.add(2);
            System.out.println(numbers);
            assertEquals(2, numbers.size());
            assertEquals(1, numbers.get(0));
            assertEquals(2, numbers.get(1));
        }

    }
