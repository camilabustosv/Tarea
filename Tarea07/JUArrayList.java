
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

    public class JUArrayList<Integer> implements List<Integer>{
        private Integer[] array;
        private int count;

        public JUArrayList(){
            this.array = (Integer[]) new Object[10];
            count = 0;
        }

        @Override
        public int size() {
            int sizeWithoutNulls = 0;
            for (int i = 0; i < array.length; i++){
                if (array[i] == null) break;
                else sizeWithoutNulls = i + 1;
            }
            return sizeWithoutNulls;
        }

        @Override
        public boolean isEmpty() {
            return size() == 0;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Integer> iterator() {
            return new Iterator<>() {
                int contador = 0;
                public boolean hasNext() {
                    return size() > contador;
                }
                public Integer next() {
                    Integer item = array[contador];
                    contador++;
                    return item;
                }
            };
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Integer integer) {
            try{
                add(size(), integer);
                return true;
            } catch (Exception e){
                return false;
            }
        }

        @Override
        public boolean remove(Object o) {
            try {
                for (int i = 0; i < array.length; i++){
                    if (array[i].equals(o)) {
                        remove(i);
                        break;
                    }
                }
                return true;
            } catch (Exception e){
                return false;
            }
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Integer> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends Integer> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {
            array = (Integer[]) new Object[10];
            count = 0;
        }

        @Override
        public Integer get(int index) {
            if (index < 0 || index >= size()) throw new IndexOutOfBoundsException("Not a valid index");
            return array[index];
        }

        @Override
        public Integer set(int index, Integer element) {
            return null;
        }

        @Override
        public void add(int index, Integer element) {
            if (index < 0 || index > size()) throw new IndexOutOfBoundsException("Not valid index");
            else{
                if (count == array.length) {
                    Integer[] array2 = (Integer[]) new Object[array.length + 10];
                    for (int i = 0; i < array.length; i++) {
                        array2[i] = array[i];
                    }
                    array = array2;
                }

                Integer[] tmpArray = (Integer[]) new Object[array.length + 1];
                for (int i = 0; i < array.length; i++) {
                    tmpArray[i] = array[i];
                }

                array[index] = element;
                for (int i = index + 1; i < array.length - 1; i++){
                    array[i] = tmpArray[i - 1];
                }
                count++;
            }
        }

        @Override
        public Integer remove(int index) {
            Integer removedElement = array[index];
            for (int j = index; j < count; j++) {
                array[j] = array[j + 1];
            }
            count--;
            return removedElement;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Integer> listIterator() {
            return null;
        }

        @Override
        public ListIterator<Integer> listIterator(int index) {
            return null;
        }

        @Override
        public List<Integer> subList(int fromIndex, int toIndex) {
            return null;
        }

    }
