class Node<T extends Comparable<T>>  {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "[" + data + "|*] -> "  + next ;
    }

    public static Node get(Node node, int index){
        if(index ==0){
            return node;
        }
        return get(node.next,index -1);
    }
}
