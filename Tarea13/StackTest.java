import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {

    @Test
    void pushTest() {
        Stack<Integer> stackList = new Stack<>();
        stackList.push(3);
        assertEquals(3,stackList.get(0));
        stackList.push(5);
        assertEquals(5,stackList.get(0));
        assertEquals(3,stackList.get(1));
    }
    @Test
    void pushStringTest() {
        Stack<String> stackList = new Stack<>();
        stackList.push("a");
        assertEquals("a",stackList.get(0));
        stackList.push("b");
        assertEquals("b",stackList.get(0));
        assertEquals("a",stackList.get(1));
    }

    @Test
    void popTest() {
        Stack<Integer> stackList2 = new Stack<>();
        stackList2.push(3);
        stackList2.push(5);
        stackList2.push(4);
        Integer dataPop= stackList2.pop();
        assertEquals(4,dataPop);
        assertEquals(5,stackList2.get(0));
        assertEquals(3,stackList2.get(1));
        assertEquals(null,stackList2.get(2));

    }
    @Test
    void popStringTest() {
        Stack<String> stackList2 = new Stack<>();
        stackList2.push("a");
        stackList2.push("b");
        stackList2.push("c");
        String dataPop= stackList2.pop();
        assertEquals("c",dataPop);
        assertEquals("b",stackList2.get(0));
        assertEquals("a",stackList2.get(1));
        assertEquals(null,stackList2.get(2));

    }
}