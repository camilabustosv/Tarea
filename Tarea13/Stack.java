public class Stack<T extends Comparable<T> >{
    private Node<T> root;
    public void push(T data){
        Node <T> newNode= new Node<T>(data);
        newNode.setNext(root);
        root= newNode;
    }
    public T pop(){
        Node<T> nextRoot = root.getNext();
        T removedData = root.getData();
        root= nextRoot;
        return removedData;
    }
    public T get(int index){
        Node<T> auxNode =Node.get(root,index);
       return auxNode != null ? auxNode.getData() : null;
    }
}
