public class Queque {
   private Node head;
   private Node tail;
   private int size;

     public void enqueque(int data) {

         Node node = new Node(data);
         if (size == 0) {
             head = node;
             tail = node;
         } else {
             node.setNext(tail);
             tail = node;
         }
         size ++;
     }

    @Override
    public String toString() {
        return "Queque{" +
                "head=" + head +
                ", tail=" + tail +
                ", size=" + size +
                '}';
    }

    public int deque(){
        int dataRemoved = head.getData();
        if(head != null){
            head = head.getNext();
        }
        size --;
        return dataRemoved;
     }

    }

