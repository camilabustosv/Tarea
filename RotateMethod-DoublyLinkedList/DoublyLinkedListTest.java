import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class DoublyLinkedListTest {

    @Test
    public void sizeTest(){
       DoublyLinkedList<Integer> dlist= new DoublyLinkedList();
       dlist.add(2);
       dlist.add(5);
        assertEquals(2,dlist.size());
    }
    @Test
    public void sizeTest2(){
        DoublyLinkedList<String> dlist2= new DoublyLinkedList();
        dlist2.add(2);
        dlist2.add(5);
        assertEquals(2,dlist2.size());
    }
    @Test
    public void emptyTest(){
        DoublyLinkedList<String> dlist3= new DoublyLinkedList();
        assertEquals(0,dlist3.size());
    }
    @Test
    public void emptyTest2(){
        DoublyLinkedList<Integer> dlist4= new DoublyLinkedList();
        assertEquals(0,dlist4.size());
    }
    @Test
    public void getTest(){
        DoublyLinkedList<Integer> dlist5= new DoublyLinkedList();
        dlist5.add(9);
        dlist5.add(7);
        dlist5.add(8);
        dlist5.add(3);
        dlist5.add(6);

        assertEquals(3,dlist5.get(1));
    }
    @Test
    public void getTest2(){
        DoublyLinkedList<String> dlist6= new DoublyLinkedList();
        dlist6.add("s");
        dlist6.add("r");
        dlist6.add("p");
        dlist6.add("u");
        dlist6.add("w");

        assertEquals("u",dlist6.get(1));
    }
    @Test
    public void mergeSortTest(){

        List<Integer> dlist7 = new DoublyLinkedList();
        dlist7.add(5);
        dlist7.add(1);
        dlist7.add(2);
        dlist7.add(3);
        dlist7.mergeSort();
        List<Integer> dlist7ordered = new DoublyLinkedList();
        dlist7ordered.add(5);
        dlist7ordered.add(3);
        dlist7ordered.add(2);
        dlist7ordered.add(1);
        assertEquals(dlist7.toString(),dlist7ordered.toString());
    }

    @Test
    public void rotateTestInteger(){
        List<Integer> dlist8 = new DoublyLinkedList();
        dlist8.add(5);
        dlist8.add(1);
        dlist8.add(2);
        dlist8.add(3);
        dlist8.rotate(2);
        System.out.println(dlist8);
        List<Integer> dlist8rotated = new DoublyLinkedList();
        dlist8rotated.add(2);
        dlist8rotated.add(3);
        dlist8rotated.add(5);
        dlist8rotated.add(1);
        System.out.println(dlist8rotated);
        assertEquals(dlist8.toString(),dlist8rotated.toString());

    }
    @Test
    public void rotateTestString(){
        List<String> dlist9 = new DoublyLinkedList();
        dlist9.add("a");
        dlist9.add("b");
        dlist9.add("c");
        dlist9.add("d");
        dlist9.rotate(3);
        System.out.println(dlist9);
        List<String> dlist9rotated = new DoublyLinkedList();
        dlist9rotated.add("b");
        dlist9rotated.add("c");
        dlist9rotated.add("d");
        dlist9rotated.add("a");
        System.out.println(dlist9rotated);
        assertEquals(dlist9.toString(),dlist9rotated.toString());

    }



}