
public  class Node<T extends Comparable<T>>  {
    private T data;
    private Node next;
    private Node prev;
    public Node(T value) {
        data = value;
        next = prev = null;
    }

    public Comparable<T> getData() {
        return data;
    }
    public Node getNext() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }
    public Node getPrev() {
        return prev;
    }
    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public String toString(){
        Comparable<T> prevString = null;
        if (prev != null){
            prevString = prev.getData();
        }
        return "{data=" + data + ", prev=" + prevString + ", next->" + next + "}";
    }


}

