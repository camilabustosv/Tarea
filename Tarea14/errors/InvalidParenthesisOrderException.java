package Tarea14.errors;

public class InvalidParenthesisOrderException extends Exception{
    private static final String defaultMessage = "Invalid parenthesis order provided";
    public InvalidParenthesisOrderException() {
        super(defaultMessage);
    }
    public InvalidParenthesisOrderException(String errorMessage) {
        super(errorMessage);
    }
}
