package Tarea14;

public class OperationHelper {
    public static int factorial(int number) {
        return number == 0 ? 1 : number * factorial(number - 1);
    }
    public static int calculate(int leftNumber, int rightNumber, char operation) {
        switch (operation) {
            case '^': return (int) Math.pow(leftNumber, rightNumber);
            case '+': return leftNumber + rightNumber;
            case '*': return leftNumber * rightNumber;
        }
        return 0;
    }
}
