package Tarea14;

import Tarea14.errors.InvalidOperatorException;
import Tarea14.errors.InvalidParenthesisOrderException;
import Tarea14.stack.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorHelperTest {
    @Test
    void validateParenthesis() {
        Stack<Character> stack = StackHelper.getTokens("3+(4+(8+2)()");
        Exception exception = assertThrows(InvalidParenthesisOrderException.class, () -> ValidatorHelper.validateParenthesis(stack));
        String expected="Invalid parenthesis order provided";
        String message=exception.getMessage();
        assertEquals(expected, message);
    }
    @Test
    void validateParenthesisNotThrow() {
        Stack<Character> stack = StackHelper.getTokens("3+(4+(8+2))");
        assertDoesNotThrow(() -> ValidatorHelper.validateParenthesis(stack));
    }
    @Test
    void validateInvalidOperators() {
        Stack<Character> stack = StackHelper.getTokens("3-(4-(8+2))");
        Exception exception = assertThrows(InvalidOperatorException.class, () -> ValidatorHelper.validateValidOperators(stack));
        String expected="Invalid operators detected.";
        String message=exception.getMessage();
        assertEquals(expected, message);
    }
    @Test
    void validateValidOperators() {
        Stack<Character> stack = StackHelper.getTokens("3+(4+(8+2))");
        assertDoesNotThrow(() -> ValidatorHelper.validateValidOperators(stack));

    }


}