package Tarea14;

import Tarea14.stack.Node;
import Tarea14.stack.Stack;

public class Calculator {
    Node<Character> priorityOperators;
    public Calculator() {
        priorityOperators = new Node<>('^', new Node<>('+', new Node('*')));
    }

    public int calculate(String operation) throws Exception {
        Stack<Character> tokens = StackHelper.getTokens(operation);
        validate(tokens);
        return calculate(tokens);
    }
    private int calculate(Stack<Character> tokens) {
        while (StackHelper.hasCharacter(tokens, '(')) {
            Stack extracted = StackHelper.extractParenthesis(tokens);
            int value = calculate(extracted);
            Stack<Character> valueTokens = StackHelper.getTokens(value + "");
            tokens = StackHelper.replaceParenthesis(tokens, valueTokens);
        }

        Stack<Integer> numbers = StackHelper.getNumbers(tokens);
        Stack<Character> operators = StackHelper.getOperators(tokens);
        int value = eval(numbers, operators, priorityOperators);
        return value;
    }

    private int eval(Stack<Integer> numbers, Stack<Character> operators, Node<Character> priorities) {
        Stack<Integer> auxNumbers = new Stack<>();
        Stack auxOperators = new Stack<>();
        while (!numbers.isEmpty()) {
            Character operator = operators.pop();
            int leftNumber = numbers.pop();
            if (operator == priorities.getData()) {
                int rightNumber = numbers.pop();
                int result = OperationHelper.calculate(leftNumber, rightNumber, operator);
                numbers.push(result);
            } else {
                auxNumbers.push(leftNumber);
                if (operator != null) {
                    auxOperators.push(operator);
                }
            }
        }
        if(auxNumbers.size() == 1) return auxNumbers.pop();
        return eval(StackHelper.reverse(auxNumbers), StackHelper.reverse(auxOperators), priorities.getNext());
    }

    private void validate(Stack<Character> tokens) throws Exception {
        ValidatorHelper.validateParenthesis(tokens);
        ValidatorHelper.validateValidOperators(tokens);
    }

}



