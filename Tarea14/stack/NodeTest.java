package Tarea14.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {
    @Test
    public void getSizeTest(){
        Node node = new Node<>(2);
        node.setNext(new Node(3));
        node.getNext().setNext(new Node(5));
        node.getNext().getNext().setNext(new Node(4));
        assertEquals(4,Node.getSize(node));

    }
    @Test
    public void getNodeTest(){
        Node node = new Node(2,new Node(1,new Node<>(7, new Node<>(6))));
        Node nodeFound = Node.getNode(node,1);
        assertEquals(1,nodeFound.getData());
    }
}