package Tarea14.stack;

public class Stack<T extends Comparable<T>> {
    private Node root;
    public String toString() { return "root=" + root; }
    public boolean isEmpty() {
        return root == null;
    }
    public int size() {
        return Node.getSize(root);
    }
    public T get(int pos) {
        Node node = Node.getNode(root, pos);
        return (T) node.getData();
    }
    public void push(T data) {
        Node top = new Node(data);
        top.setNext(root);
        root = top;
    }
    public T pop() {
        if (root == null) return null;
        T data = (T) root.getData();
        root = root.getNext();
        return data;
    }
}
