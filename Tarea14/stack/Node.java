package Tarea14.stack;

public class Node<T extends Comparable<T>> {
    private T data;
    private Node next;
    public Node(T data) {
        this.data = data;
        next = null;
    }
    public Node(T data, Node next) {
        this.data = data;
        this.next = next;
    }
    public T getData() { return data; }
    public Node getNext() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }
    public String toString() {
        return data + " -> " + next;
    }
    public static int getSize(Node node) {
        int counter = 0;
        while (node != null) {
            counter++;
            node = node.getNext();
        }
        return counter;
    }


    public static Node getNode(Node node, int pos) {
        return pos == 0 ? node : getNode(node.getNext(), pos - 1);
    }
}
