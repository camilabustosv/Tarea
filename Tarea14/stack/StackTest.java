package Tarea14.stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StackTest {

    @Test
    void push() {
        Stack list = new Stack();
        list.push(5);
        assertEquals(1, list.size());
        list.push(3);
        assertEquals(2, list.size());
    }

    @Test
    void pop() {
        Stack<Integer> list = new Stack();
        list.push(5);
        list.push(3);
        list.push(1);
        assertEquals(3, list.size());
        int data = list.pop();
        assertEquals(2, list.size());
        assertEquals(1, data);
    }

}