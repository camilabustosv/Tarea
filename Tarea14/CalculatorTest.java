package Tarea14;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void calculateOnlyAdditionsTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("1+2+3");
        assertEquals(6,value);
    }
    @Test
    void calculateMultiplicationTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("2*2*5");
        assertEquals(20,value);
    }
    @Test
    void calculateExponentialOperatorsMixTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("2^3+2");
        assertEquals(10,value);
    }
    @Test
    void calculateAdditionOperatorsMixTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("1+2*5");
        assertEquals(15,value);
    }
    @Test
    void calculateFactorialTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("5! + 10");
        assertEquals(130,value);
    }
    @Test
    void calculateFactorialTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("5! + 10");
        assertEquals(130,value);
    }
    @Test
    void calculateFactorialExponentialTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("3!^2");
        assertEquals(36,value);
    }
    @Test
    void calculateMixOperators2Test() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("4^2*3");
        assertEquals(24,value);
    }

    @Test
    void calculateMixOperatorsTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("3^3!+5*2");
        assertEquals(1468,value);
    }
    @Test
    void calculateMixOperatorsParenthesisTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("3^3!+(5*2)");
        assertEquals(739,value);
    }
    @Test
    void calculateFactorialParenthesisTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("!(3 + 2)");
        assertEquals(120,value);
    }
    @Test
    void calculateMultiplicationParenthesisTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("(1+2)*5");
        assertEquals(15,value);
    }
    @Test
    void calculateMixOperatorParenthesisTest() throws Exception {
        Calculator cal = new Calculator();
        int value = cal.calculate("2^(3+2)");
        assertEquals(64,value);
    }









}