package Tarea14;

import Tarea14.stack.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackHelperTest {

    @Test
    void reverseTest() {
        Stack<Character> tokens = StackHelper.getTokens("2+3");
        tokens = StackHelper.reverse(tokens);
        assertEquals('3',tokens.get(0));
        assertEquals('+',tokens.get(1));
        assertEquals('2',tokens.get(2));

    }

    @Test
    void cloneTest() {
        Stack<Character> tokens = StackHelper.getTokens("4^2*3");
        Stack<Character> cloned =  StackHelper.clone(tokens);
        assertEquals('4',cloned.get(0));
        assertEquals('^',cloned.get(1));
        assertEquals('2',cloned.get(2));
        assertEquals('*',cloned.get(3));
        assertEquals('3',cloned.get(4));
        assertNotEquals(System.identityHashCode(tokens),System.identityHashCode(cloned));
    }

    @Test
    void getTokens() {
        Stack<Character> tokens = StackHelper.getTokens("4^(2*3)+3");
        assertEquals(9, tokens.size());
    }

    @Test
    void getNumbers() {
        Stack<Character> tokens = StackHelper.getTokens("4^3!*3+11");
        Stack<Integer> numbers = StackHelper.getNumbers(tokens);
        assertEquals(4,numbers.get(0));
        assertEquals(6,numbers.get(1));
        assertEquals(3,numbers.get(2));
        assertEquals(11,numbers.get(3));
    }

    @Test
    void getOperators() {
        Stack<Character> tokens = StackHelper.getTokens("4^3!*3+11");
        Stack<Character> operators = StackHelper.getOperators(tokens);
        assertEquals('^',operators.get(0));
        assertEquals('*',operators.get(1));
        assertEquals('+',operators.get(2));

    }

    @Test
    void hasCharacter() {
        Stack<Character> tokens = StackHelper.getTokens("4^3!*3+11");
        assertTrue(StackHelper.hasCharacter(tokens,'^'));
        assertFalse(StackHelper.hasCharacter(tokens,'-'));

    }

    @Test
    void extractParenthesisTest() {
        Stack<Character> tokens = StackHelper.getTokens("4^(2*3)+3");
        Stack<Character> extracted = StackHelper.extractParenthesis(tokens);
        assertEquals('2',extracted.get(0));
        assertEquals('*',extracted.get(1));
        assertEquals('3',extracted.get(2));
        assertEquals(3,extracted.size());
    }
    @Test
    void replaceParenthesisTest() {
        Stack<Character> tokens = StackHelper.getTokens("4^(2*3)+3");
        tokens = StackHelper.replaceParenthesis(tokens, StackHelper.getTokens("56"));
        assertEquals('5',tokens.get(2));
        assertEquals('6',tokens.get(3));
        assertFalse(StackHelper.hasCharacter(tokens,'('));
        assertFalse(StackHelper.hasCharacter(tokens,')'));
        assertEquals(6,tokens.size());

    }



}