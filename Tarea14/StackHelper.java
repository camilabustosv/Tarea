package Tarea14;

import Tarea14.stack.Stack;

public class StackHelper {
    public static Stack reverse(Stack stack) {
        Stack reversed = new Stack();
        while (!stack.isEmpty()) {
            reversed.push(stack.pop());
        }
        return reversed;
    }
    public static Stack clone(Stack stack) {
        Stack cloned = new Stack();
        int pos = 0;
        while (pos < stack.size()) {
            cloned.push(stack.get(pos));
            pos++;
        }
        return reverse(cloned);
    }
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = str.length() - 1; i >= 0; i--) {
            char token = str.charAt(i);
            if (token == ' ') continue;
            tokens.push(token);
        }
        return tokens;
    }
    // 2! ^ 3 * 534
    public static Stack<Integer> getNumbers(Stack<Character> tokens) {
        Stack<Integer> numbers = new Stack<>();
        String aux = "";
        int pos = 0;
        while (pos < tokens.size()) {
            char token = tokens.get(pos);
            if (Character.isDigit(token)) {
                aux = aux + token;
            } else if (aux.length() > 0) {
                /* long form
                int num = Integer.parseInt(aux);
                if (token == '!') {
                    int factorial = OperationHelper.factorial(num);
                    numbers.push(factorial);
                } else {
                    numbers.push(num);
                }*/
                numbers.push(token == '!' ? OperationHelper.factorial(Integer.parseInt(aux)) : Integer.parseInt(aux));
                aux = "";
            }
            pos++;
        }
        if (aux.length() > 0) {
            numbers.push(Integer.parseInt(aux));
        }
        return reverse(numbers);
    }
    public static Stack<Character> getOperators(Stack<Character> tokens) {
        Stack<Character> auxTokens = clone(tokens);
        Stack<Character> operators = new Stack<>();
        while (!auxTokens.isEmpty()) {
            char token = auxTokens.pop();
            if (!Character.isDigit(token) && token != '!') {
                operators.push(token);
            }
        }
        return reverse(operators);
    }
    public static boolean hasCharacter(Stack<Character> stack, char c) {
        int pos = 0;
        while (pos < stack.size()) {
            char token = stack.get(pos);
            if (token == c) return true;
            pos++;
        }
        return false;
    }
    public static Stack extractParenthesis(Stack<Character> stack) {
        Stack<Character> extracted = new Stack<>();
        int found, pos;
        found = pos = 0;
        while (pos < stack.size()) {
            char token = stack.get(pos);
            if (token == '(') {
                found++;
            }
            if (token == ')') {
                if (found == 1) break;
                found--;
            }
            if (found > 0 && token != '(') extracted.push(token);
            pos++;
        }
        return reverse(extracted);
    }
    public static Stack replaceParenthesis(Stack<Character> stack, Stack<Character> data) {
        Stack<Character> reduced = new Stack<>();
        int found = 0;
        boolean ended = false;
        while (!stack.isEmpty()) {
            char token = stack.pop();
            if (token == '(') {
                found++;
            }
            if (token == ')') {
                found--;
                if (found == 0) ended = true;
            }
            if (found <= 0 && token != ')') reduced.push(token);
            if (ended) {
                while (!data.isEmpty()) {
                    reduced.push(data.pop());
                }
                ended = false;
            }
        }
        return reverse(reduced);
    }
}
