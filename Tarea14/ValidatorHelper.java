package Tarea14;

import Tarea14.errors.InvalidOperatorException;
import Tarea14.errors.InvalidParenthesisOrderException;
import Tarea14.stack.Stack;

public class ValidatorHelper {
    public static void validateParenthesis(Stack<Character> stack) throws InvalidParenthesisOrderException {
        int pos = 0;
        Stack<Character> parenthesis = new Stack<>();
        while (pos < stack.size()) {
            char token = stack.get(pos);
            if (token == '(') parenthesis.push(token);
            if (token == ')') parenthesis.push(token);
            pos++;
        }
        Stack<Character> reversed = StackHelper.reverse(parenthesis);
        int counter = pos = 0;
        while (pos < reversed.size()) {
            char token = reversed.pop();
            if (token == '(') counter++;
            if (token == ')') counter--;
            if (counter < 0) throw new InvalidParenthesisOrderException();
        }
        if (counter != 0 ) throw new InvalidParenthesisOrderException();
    }
    public static void validateValidOperators(Stack<Character> stack) throws InvalidOperatorException {
        int pos = 0;
        while (pos < stack.size()) {
            char token = stack.get(pos);
            if (token == ' ') continue;
            if (!Character.isDigit(token) && token != '(' && token != ')' && token != '!' && token != '*' && token != '+'&& token != '^') {
                throw new InvalidOperatorException();
            }
            pos++;
        }
    }

}