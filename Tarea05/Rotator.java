
public class Rotator {
    public Object[] rotate(Object[] data, int n) {
        int j=0;
        while (n>0){
            int temp = (int) data[data.length -1];
            for(j = data.length -1;j>0;j--){
                data[j]= data[j -1];
            }
            data[j] = temp;
            n --;
        }
        return  data[j];
    }

}
